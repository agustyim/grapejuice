import pytest


@pytest.fixture()
def a_launch_uri():
    return "roblox-player:1+launchmode:play+gameinfo:TEST_GAME_INFO+launchtime:1692294086755+placelauncherurl:https%3A%2F%2Fassetgame.roblox.com%2Fgame%2FPlaceLauncher.ashx%3Frequest%3DRequestGame%26browserTrackerId%23452345345%26placeId%45454545%26isPlayTogetherGame%3Dfalse%26joinAttemptId%ASOIDJASDOIJAIOSJD%26joinAttemptOrigin%3DPlayButton+browsertrackerid:123123123123+robloxLocale:en_us+gameLocale:en_us+channel:"

@pytest.fixture()
def a_launch_uri_no_channel():
    return "roblox-player:1+launchmode:play+gameinfo:TEST_GAME_INFO+launchtime:1692294086755+placelauncherurl:https%3A%2F%2Fassetgame.roblox.com%2Fgame%2FPlaceLauncher.ashx%3Frequest%3DRequestGame%26browserTrackerId%23452345345%26placeId%45454545%26isPlayTogetherGame%3Dfalse%26joinAttemptId%ASOIDJASDOIJAIOSJD%26joinAttemptOrigin%3DPlayButton+browsertrackerid:123123123123+robloxLocale:en_us+gameLocale:en_us"
